from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from django.views import generic
from rest_framework.viewsets import ModelViewSet
from rest_framework import response,status
from .models import *
from .serializers import item_serializer

def home(request):
    context={}
    return render(request,"index.html",context)

# def restaurant(request):
#     context={}
#     return render(request,"restaurant.html",context)


class items_viewset(ModelViewSet):
    queryset = item_list.objects.all()
    serializer_class = item_serializer

    def create(self, request, *args, **kwargs):
        try:
            item_ob = item_list(name_of_item=request.data['name_of_item'])
            if 'description' in request.data:
                item_ob.description = request.data['description']
            item_ob.save()
            return response.Response('item created successfully', status=status.HTTP_200_OK)
        except Exception as e:
            return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        try:
            item_ob = self.get_object()
            if 'name_of_item' in request.data:
                item_ob.name_of_item = request.data['name_of_item']
            if 'description' in request.data:
                item_ob.description = request.data['description']
            return response.Response('item updated successfully', status=status.HTTP_200_OK)
        except Exception as e:
            return response.Response({"error": e.message}, status=status.HTTP_400_BAD_REQUEST)



from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from django.contrib.auth.models import User
from serializers import *
from .models import *
from django.contrib.sessions.models import Session
from rest_framework.decorators import detail_route
from django.contrib.auth import authenticate, login, logout
from rest_framework import status, viewsets
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token
from django.conf import settings
# imports from settings.py
from landproject.settings import *
from rest_framework import response,status
# Permission classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.authentication import BasicAuthentication

from django.contrib.auth.signals import user_logged_out
from rest_framework_jwt.settings import api_settings
from datetime import datetime, timedelta
import jwt
import json, requests, ast
import datetime

#mail sending plugins
from django.template import Context, RequestContext
from django.template.loader import get_template
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string

import django_filters
from  rest_framework import filters

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

import urllib
import json

def create_token(userid):
	payload = {
	'sub': str(userid),
		'iat': datetime.datetime.now(),
		'exp': datetime.datetime.now() + timedelta(days=3000)
	}
	token = jwt.encode(payload, SECRET_KEY)
	return token.decode('unicode_escape')


class LoginViewset(ViewSet):
	def create(self, request):
		# city, state = GetCurrentLocation(request)
		# request_ip = get_client_ip(request)

		# checking input put field are equal to database fields
		allowed_fields = ['email', 'password']
		input_keys = request.data.keys()
		temp = set(input_keys) - set(allowed_fields)

		if len(temp):
			temp_string = ','.join(str(x) for x in temp)
			return Response({'error': 'invalid fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		# implementing mandatory fields
		mandatory_temp = set(allowed_fields) - set(input_keys)
		if len(mandatory_temp):
			temp_string = ','.join(str(x) for x in allowed_fields)
			return Response({'error': 'mandatory fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		try:
			user = authenticate(username=request.data['email'], password=request.data['password'])
			user_default_obj = User.objects.get(username=request.data['email'])
			if user is not None and user_default_obj.is_active:
				login(request, user)
				if request.user.is_authenticated():
					session = Session.objects.filter(session_key=request.session.session_key)
					if session:
						user_session = UserSessions.objects.create(user=request.user, session=session[0])
				else:
					return Response({"message": "Invalid password. please try again"}, status=400)

				payload = jwt_payload_handler(user)
				pbs_user = pbsuser.objects.get(user_id=user.id)
				if api_settings.JWT_ALLOW_REFRESH:
					payload['orig_iat'] = timegm(
						datetime.utcnow().utctimetuple()
					)

				token = jwt_encode_handler(payload)
				user_data = {}
				user_data['first_name'] = user.first_name
				user_data['last_name'] = user.last_name
				user_data['mobile'] = pbs_user.mobile
				user_data['cover_image'] = pbs_user.cover_image
				user_data['user_id'] = pbs_user.id
				user_data['email'] = user.email

				request.session['auth_user_id'] = user.id
				request.session['pbs_id'] = pbs_user.id
				print ("request session", request.session)

				return Response({'token': token,
								 'user': [user_data],

								 'groups': user.groups.all().values_list('name', flat=True),

								 'message': 'User successfully logged in'}, status=200)

			# return Response({"message":"User logged in successfully"})

		except Exception as e:
			print ("error", e)
			return Response({"message": "Email does not exist"}, status=400)



class LogoutViewset(ViewSet):
	def list(self, request):
		try:
			user = getattr(request, 'user', None)
			if hasattr(user, 'is_authenticated') and not user.is_authenticated():
				user = None
			user_logged_out.send(sender=user.__class__, request=request, user=user)

			request.session.flush()
			if hasattr(request, 'user'):
				from django.contrib.auth.models import AnonymousUser
				request.user = AnonymousUser()
				return Response({"success": "Logged out successfully !"}, status=200)
		except Exception as e:
			return Response({"error": e.message}, status=400)


class pbuser_viewset(viewsets.ModelViewSet):
	queryset = pbsuser.objects.all()
	serializer_class = user_serializer

	def update(self, request, pk):
		try:
			# checking input put field are equal to database fields
			allowed_fields = ['id', 'first_name', 'last_name', 'password', 'email',

							  'confirm_password', 'current_password', 'mobile', 'address','deactivate']

			input_keys = request.data.keys()
			temp = set(input_keys) - set(allowed_fields)

			if len(temp):
				temp_string = ','.join(str(x) for x in temp)
				return Response({'error': 'invalid fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# checking password and confirm password field values
			pb_object = pbsuser.objects.get(id=pk)
			password = request.data.get('password', None)
			print 'asdadsa'
			c_password = request.data.get('confirm_password', None)
			if password and c_password:
				if password != c_password:
					return Response({'error': 'password and confirm password did not match'},
									status=status.HTTP_400_BAD_REQUEST)
				else:
					current_user = User.objects.get(id=pb_object.user.id)
					print (current_user.id)
					success = current_user.check_password(request.data['current_password'])

					if success:
						# current_user.set_password((request.data['new_password']))
						# print request.data['newpassword']
						current_user.set_password(request.data['password'])
						current_user.save()
						return Response({'message': 'Your Password has been changed successfully'},
										status=status.HTTP_200_OK)

					else:
						return Response({'message': 'Incorrect current password'}, status=status.HTTP_400_BAD_REQUEST)

			try:
				if 'user' in request.data:
					user_obj = request.data['user']
					user = User.objects.get(id=user_obj['id'])
				else:
					user_details = pbsuser.objects.get(id=int(pk))
					user = User.objects.get(id=user_details.user.id)
				# user = User.objects.get(id=user_obj.id)
			except Exception as e:
				print (e)
			# return Response({'error': 'no user found in users table'}, status=status.HTTP_400_BAD_REQUEST)
			try:
				user_details = pbsuser.objects.get(id=request.data['id'])
			except Exception as e:
				print (e)
				user_details = pbsuser.objects.get(id=int(pk))
				user = User.objects.get(id=user_details.user.id)
			# return Response({'error': 'no user found in users table'}, status=status.HTTP_400_BAD_REQUEST)

			if 'user' in request.data:
				user_obj = request.data['user']
				user.first_name = user_obj['first_name']
				user.last_name = user_obj['last_name']

			if 'mobile' in request.data:
				user_details.mobile = request.data['mobile']

			if 'deactivate' in request.data:
				if request.data['deactivate'] != False:
					print 'aaa'
					user.is_active = False



			if 'address' in request.data:
				user_details.address = request.data['address']

			if 'gender' in request.data:
				user_details.gender = request.data['gender']

			if 'cover_image' in request.data:
				cover_image = request.data['cover_image']
				try:
					res = cloudinary.uploader.upload(cover_image)
					user_details.cover_image = res['url']
				except Exception, e:
					print (e)

			user.save()
			user_details.save()
			return Response({'response': 'successfully updated user information'}, status=status.HTTP_200_OK)
		except Exception as e:
			print (e)
			return Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)


	def create(self, request, *args, **kwargs):
		# checking input put field are equal to database fields
		allowed_fields = ['first_name', 'last_name','group_name', 'email', 'password', 'mobile', 'cover_image', 'profile_pic','username',"role"]
		mandatory_fields = ['email', 'password']
		input_keys = request.data.keys()
		temp = set(input_keys) - set(allowed_fields)

		if len(temp):
			temp_string = ','.join(str(x) for x in temp)
			return Response({'error': 'invalid fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		# implementing mandatory fields
		mandatory_temp = set(mandatory_fields) - set(input_keys)
		if len(mandatory_temp):
			temp_string = ','.join(str(x) for x in mandatory_temp)
			return Response({'error': 'mandatory fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		try:
			user = User.objects.create_user(request.data['email'], request.data['email'], request.data['password'])
			user.is_active = True
			try:
				if 'first_name' in request.data:
					user.first_name = request.data['first_name']
				if 'last_name' in request.data:
					user.last_name = request.data['last_name']
				user.save()
				user_details = pbsuser(user=user)
				if 'mobile' in request.data:
					user_details.mobile = request.data['mobile']
				if 'cover_image' in request.data:
					user_details.cover_image = request.data['cover_image']



				user_details.save()
				if 'group_name' in request.data:
					c_group = Group.objects.get(name=request.data['group_name'])
					user.groups.add(c_group)
				# token = create_token(user.id)

				token = jwt.encode({
					'username': user.username,
					'iat': datetime.datetime.utcnow(),
					'nbf': datetime.datetime.utcnow() + datetime.timedelta(minutes=-5),
					'exp': datetime.datetime.utcnow() + datetime.timedelta(days=7)
				}, settings.SECRET_KEY)


				user_details.save()
				return Response({'success': {'message': 'You have been signup successfully !',
											 "id": user_details.id,"email": request.data['email']}},
								status=status.HTTP_200_OK)
			except Exception as e:
				print(e)
				return Response({'message': 'Email already exists'}, status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print(e)
			return Response({'message': 'It seems you already have an account !'}, status=status.HTTP_400_BAD_REQUEST)

from django.contrib.auth.models import Group
class group_viewset(viewsets.ModelViewSet):
	queryset = Group.objects.all()
	serializer_class = group_serializer


def send_activation_mail(request,data, token, mail_type):
	# content = Context({
	# 	"email": str(data['email']),
	# 	"activationcode": token,
	# 	"server_url": request.get_host()
	# })
	ctx = {
				"email": str(data['email']),
				"token": token,
				"server_url": request.get_host()
			}
	if mail_type == 'activation_mail':
		# activation template
		subject = "Account Activation"
		pwdtemplate = get_template('registeremail.html')
		htmlt = render_to_string('confirmaccount.html', ctx)
	elif mail_type == 'signup_mail':
		# forgot template
		subject = "Welcome to Property Booking System"
		pwdtemplate = get_template('confirmaccount.html')
		htmlt = render_to_string('confirmaccount.html', ctx)

	elif mail_type == 'forgot_mail':
		subject = "Password Reset Request"
		# pwdtemplate = get_template('forgotpassword.html')
		htmlt = render_to_string('forgotpassword.html',ctx)
		# htmlt = render_to_string('confirmaccount.h
    # tml', ctx)

	else:
		pwdtemplate = get_template('send_activation.html')
	# htmlt = pwdtemplate.render(content)
	to = [str(data['email'])]
	by = ""
	msg = EmailMultiAlternatives(subject, "", by, to)
	msg.attach_alternative(htmlt, "text/html")
	msg.send()

from django.contrib.auth.models import User
class ForgotPasswordViewset(ViewSet):
	def create(self,request):
		try:
			# ---- parameter verifications ----
			allowed_fields = ['email']
			input_keys = request.data.keys()
			temp = set(input_keys) - set(allowed_fields)
			if len(temp):
				temp_string = ','.join(str(x) for x in temp)
				return Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# implementing mandatory fields
			mandatory_temp = set(allowed_fields) - set(input_keys)
			if len(mandatory_temp):
				temp_string = ','.join(str(x) for x in allowed_fields)
				return Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			try:
				user = User.objects.get(email=request.data['email'])

			except Exception as e:
				print e
				return Response({'error': 'no account found with this email ...'}, status=status.HTTP_400_BAD_REQUEST)

			if not user:
				return Response({'error': 'no email found to this account ...'}, status=status.HTTP_400_BAD_REQUEST)
			# sending forgot password link
			obj = {'first_name': user.first_name, 'last_name': user.last_name, 'email': user.email}
			token = create_token(user.id)
			try:
				buddy = pbsuser.objects.get(user=user)
				buddy.forgot_pass_token = token
				buddy.save()
				send_activation_mail(request, obj, token, 'forgot_mail')
				return Response({'message': 'forgot password link successfully sent'}, status=status.HTTP_200_OK)
			except Exception as e:
				print (e)
				return Response({'error': 'no user account found with this email'}, status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print (e)
			return Response({'error': 'some thing went wrong ...'}, status=status.HTTP_400_BAD_REQUEST)

class ForgotPasswordProcessViewset(ViewSet):
	def create(self,request):
		try:
			# ---- parameter verifications ----
			allowed_fields = ['email', 'token', 'password']
			input_keys = request.data.keys()
			temp = set(input_keys) - set(allowed_fields)
			if len(temp):
				temp_string = ','.join(str(x) for x in temp)
				return Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# implementing mandatory fields
			mandatory_temp = set(allowed_fields) - set(input_keys)
			if len(mandatory_temp):
				temp_string = ','.join(str(x) for x in allowed_fields)
				return Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)
			try:
				user = User.objects.get(email=request.data['email'])
			except Exception as e:
				return Response({'error': 'no account found with this email'}, status=status.HTTP_400_BAD_REQUEST)
			try:
				BUser = pbsuser.objects.get(forgot_pass_token=request.data['token'], user_id=user)
				user.set_password(request.data['password'])
				user.save()
				BUser.forgot_pass_token = ''
				BUser.save()
				return Response({'message': 'successfully updated new password'}, status=status.HTTP_200_OK)
			except Exception as e:
				print e
				return Response({'error': 'no token or user found'}, status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			return Response({'error': 'no account found with this email'}, status=status.HTTP_400_BAD_REQUEST)

class product_viewset(viewsets.ModelViewSet):
	queryset = products.objects.all()
	serializer_class = product_serializer


class add_to_cart_viewset(viewsets.ViewSet):
	def create(self, request):
		user_id = request.data["user_id"]
		product_id = request.data["product_id"]
		quantity = request.data["product_quantity"]
		add_to_cart.objects.create(user=pbsuser.objects.get(id=user_id),product= products.objects.get(id=product_id),product_quantity=quantity)
		return response.Response("success, prodcut added to cart", status=status.HTTP_200_OK)

class book_the_product_viewset(viewsets.ViewSet):
	def create(self,request):
		user_id = request.data["user_id"]
		product_id = request.data["product_id"]
		quantity = request.data["product_quantity"]
		booking.objects.create(user=pbsuser.objects.get(id=user_id), product=products.objects.get(id=product_id),
								   product_quantity=quantity)
		return response.Response("success, booking done", status=status.HTTP_200_OK)

class get_my_cart_viewset(viewsets.ViewSet):
	def list(self, request):
		user_id = request.GET.get("user_id")
		queryset = add_to_cart.objects.filter(user=pbsuser.objects.get(id=user_id))
		serializer = add_to_cart_serializer(instance=queryset,many=True)
		return response.Response(serializer.data, status=status.HTTP_200_OK)

class get_my_bookings(viewsets.ViewSet):
	def list(self, request):
		user_id = request.GET.get("user_id")
		queryset = booking.objects.filter(user=pbsuser.objects.get(id=user_id))
		serializer = booking_serializer(instance=queryset, many=True)
		return response.Response(serializer.data, status=status.HTTP_200_OK)










