# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(products)
admin.site.register(pbsuser)
admin.site.register(add_to_cart)
admin.site.register(booking)

