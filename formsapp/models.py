# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session

# Create your models here.


class item_list(models.Model):
    name_of_item = models.CharField(max_length=100)
    description = models.TextField(null=True,blank=True)
    def __str__(self):
        return self.name_of_item


class pbsuser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    gender = models.CharField(max_length=50, blank=True, null=True)
    mobile = models.CharField(max_length=50, null=True, blank=True)
    social_login_image = models.TextField(default="", blank=True)
    social_login_email = models.TextField(default="", blank=True)
    activate_token = models.CharField(null=False, default="", max_length=500, blank=True)
    forgot_pass_token = models.CharField(null=False, default="", max_length=500, blank=True)
    dob = models.CharField(null=False, default="", max_length=50, blank=True)
    address = models.CharField(null=False, default="", max_length=500, blank=True)
    cover_image = models.CharField(null=False, default="", max_length=500, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.user)


class UserSessions(models.Model):
    user = models.ForeignKey(User, related_name='user_sessions')
    session = models.ForeignKey(Session, related_name='user_sessions',
                                on_delete=models.CASCADE)

    def __str__(self):
        return '%s - %s' % (self.user, self.session.session_key)

class products(models.Model):
    product_name = models.CharField(max_length=100)
    product_description = models.TextField(null=True, blank=True)
    product_price = models.IntegerField(null=True,blank=True)
    product_color = models.CharField(max_length=100, null=True,blank=True)

    def __str__(self):
        return self.product_name

class add_to_cart(models.Model):
    user = models.ForeignKey(pbsuser)
    product = models.ForeignKey(products)
    product_quantity = models.IntegerField(null=True,blank=True)

    def __str__(self):
        return self.user.user.username + ' - ' + self.product.product_name

class booking(models.Model):
    user = models.ForeignKey(pbsuser)
    product = models.ForeignKey(products)
    product_quantity = models.IntegerField(null=True,blank=True)

    def __str__(self):
        return self.user.user.username + '-' + self.product.product_name



