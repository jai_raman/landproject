
from django.conf.urls import include, url
from django.contrib import admin
from . import views
from rest_framework.routers import SimpleRouter


from .views import *

router = SimpleRouter()

login_view_set = LoginViewset.as_view({
	'post': 'create'
})
logout_view_set = LogoutViewset.as_view({
	'get': 'list'})




router.register(r'users', pbuser_viewset, 'users')

router.register(r'products', product_viewset, 'products')
router.register(r'add_to_cart', add_to_cart_viewset, 'add_to_cart')
router.register(r'book_the_product', book_the_product_viewset, 'add_to_cart')
router.register(r'get_my_cart', get_my_cart_viewset, 'get_my_cart')
router.register(r'get_my_bookings', get_my_bookings, 'get_my_bookings')


router.register(r'groups', group_viewset, 'groups')
router.register(r'forgot_pass', ForgotPasswordViewset, 'forgot_pass')



router.register(r'items',items_viewset, 'items')





urlpatterns = router.urls




urlpatterns += [
    url(r'^$',views.home,name='home'),
]
from rest_framework.routers import format_suffix_patterns, url
urlpatterns += format_suffix_patterns([
    url(r'^login$',login_view_set, name='login-view-set'),
    url(r'^logout$',logout_view_set, name='logout-view-set'),])


