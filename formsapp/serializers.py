from .models import *
from rest_framework.serializers import ModelSerializer

class item_serializer(ModelSerializer):
    class Meta:
        model = item_list
        fields = '__all__'


class user_serializer(ModelSerializer):
    class Meta:
        model = pbsuser
        fields = '__all__'
        depth = 2

from django.contrib.auth.models import Group
class group_serializer(ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'

class product_serializer(ModelSerializer):
    class Meta:
        model = products
        fields = '__all__'

class add_to_cart_serializer(ModelSerializer):
    user = user_serializer()
    product = product_serializer()

    class Meta:
        model = add_to_cart
        fields = '__all__'

class booking_serializer(ModelSerializer):
    user = user_serializer()
    product = product_serializer()
    class Meta:
        model = booking
        fields = '__all__'
